package log

import (
	"fmt"
	"runtime"
)

// New returns a LoggerFunc instance
func New() LoggerFunc {
	var logger Log
	return func(level Level, message string, data interface{}) Log {
		_, fn, line, _ := runtime.Caller(1)
		caller := fmt.Sprintf("%s line %v", fn, line)
		return append(logger, newlog(level, caller, message, data))
	}
}

// LoggerFunc ...
type LoggerFunc func(level Level, message string, data interface{}) Log

// Log containing a list of arrays of log entries
type Log []*log

type log struct {
	Level   string
	Caller  string
	Message string
	Time    string
	Data    interface{}
}

func newlog(level Level, caller, message string, data interface{}) *log {
	return &log{
		Caller:  caller,
		Level:   levelString[level],
		Message: message,
		Data:    data,
	}
}
