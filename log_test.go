package log_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-helpers/log"
)

func Test_LogFunc(t *testing.T) {
	logger := log.New()
	results := logger(log.LevelInfo, "HELLO WORLD", nil)
	assert.Equal(t, 1, len(results))
	assert.Equal(t, "HELLO WORLD", results[0].Message)
	assert.Equal(t, nil, results[0].Data)
	assert.NotEqual(t, "", results[0].Caller)
	fmt.Println(results[0].Caller)

}
