package log

// Level defines log level
type Level int

// Level constasnts
const (
	LevelDebug = Level(iota)
	LevelInfo
	LevelWarn
	LevelError
)

var levelString = map[Level]string{
	LevelDebug: "DEBUG",
	LevelWarn:  "WARN",
	LevelInfo:  "INFO",
	LevelError: "ERROR",
}
